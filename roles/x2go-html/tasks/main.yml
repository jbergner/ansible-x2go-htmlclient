---
### These are the tasks for the x2go-html-role

- name: "Install required packages"
  apt:
    pkg:
      - bash
      - fcgiwrap
      - git
      - libcgi-fast-perl                ### Which CGI module is the right one?
      - libcgi-pm-perl
      - libcgi-psgi-perl
      - libcgi-simple-perl
      - libexpect-perl
      - libfile-touch-perl
      - libjson-perl
      - liburi-encode-perl
      - minify
      - nginx-full
      - python3-websockify
      - qt5-default
      - qt5-qmake
      - x2goclient

- name: "Create working user"
  user:
    name: "{{ working_user }}"
    password: '!'
    shell: "/bin/bash"

- name: "The following tasks shall be executed as working user"
  block:
    - name: "Checkout Git repositories"
      git:
        repo: "{{ item.repo }}"
        dest: "/home/{{ working_user }}/{{ item.dir }}"
      loop:
        - { repo: "git://code.x2go.org/x2gohtmlclient.git", dir: "htmlclient" }
        - { repo: "git://code.x2go.org/x2gokdriveclient.git", dir: "kdriveclient" }
        - { repo: "git://code.x2go.org/x2gowebrpc.git", dir: "webrpc" }
        - { repo: "git://code.x2go.org/x2gowswrapper.git", dir: "wswrapper" }

    - name: "Create output dir for minify-script"
      file:
        path: "/home/{{ working_user }}/htmlclient/dist"
        state: directory

    - name: "Execute minify-script"
      command: "bash minify.sh"
      args:
        chdir: "/home/{{ working_user }}/htmlclient"

    - name: "Compile wswrapper"
      block:
        - name: "qmake"
          command: "qmake"
          args:
            chdir: "/home/{{ working_user }}/wswrapper"

        - name: "make"
          make:
            chdir: "/home/{{ working_user }}/wswrapper"
  become: yes
  become_user: "{{ working_user }}"

- name: "Place resources to web directory"
  block:
    - name: "Create directories"
      file:
        path: "{{ www_document_root }}/{{ item }}"
        state: "directory"
        owner: "www-data"
        group: "www-data"
      loop:
        - "assets"
        - "assets/x2go-plugin"
        - "assets/x2go-plugin/CSS"
        - "assets/x2go-plugin/IMG"
        - "assets/x2go-plugin/JS"
        - "assets/x2go-plugin/RPC"

    - name: "Place files to web directory"
      block:
        - name: "Place individual files"
          copy:
            remote_src: yes
            src: "/home/{{ working_user }}/{{ item.src }}"
            dest: "{{ www_document_root }}/assets/x2go-plugin/{{ item.dest }}"
            owner: "www-data"
            group: "www-data"
            mode: "0640"
          loop:
            - { src: "htmlclient/CSS/x2gostyle.css", dest: "CSS/x2gostyle.css" }
            - { src: "htmlclient/src/x2gobg.svg", dest: "IMG/x2gobg.svg" }
            - { src: "htmlclient/dist/x2goclient.js", dest: "JS/x2goclient.js" }
            - { src: "webrpc/x2gorpc.cgi", dest: "RPC/x2gorpc.cgi" }
            - { src: "htmlclient/index.html", dest: "index.html" }
            - { src: "htmlclient/x2gokdriveclient.html", dest: "x2gokdriveclient.html" }

        - name: "Fix permissions for CGI-script"
          file:
            path: "{{ www_document_root }}/assets/x2go-plugin/RPC/x2gorpc.cgi"
            mode: "0750"

        - name: "Copy images"
          copy:
            remote_src: yes
            src: "{{ item }}"
            dest: "{{ www_document_root }}/assets/x2go-plugin/IMG/"
            mode: "0640"
          with_fileglob: "/home/{{ working_user }}/htmlclient/src/*.png"

- name: "Set up x2gowswrapper"
  block:
    - name: "Create log-directory"
      file:
        path: "{{ x2gows_logdir }}"
        state: directory
        owner: "www-data"

    - name: "Create config directory"
      file:
        path: "/etc/{{ item }}"
        state: directory
        mode: "0755"
      loop:
        - "x2go"
        - "x2go/x2gows"

    - name: "Set up config"
      template:
        src: "x2gows.options"
        dest: "/etc/x2go/x2gows/x2gows.options"
        owner: "www-data"
        group: "www-data"
        mode: "0640"

    - name: "Install x2gowswrapper to /usr/bin"
      copy:
        remote_src: yes
        src: "/home/{{ working_user }}/wswrapper/x2gowswrapper"
        dest: "/usr/bin/x2gowswrapper"
        owner: "root"
        group: "root"
        mode: "0755"

- name: "Set up Webserver"
  block:
    - name: "Copy FCGI-config"
      copy:
        remote_src: yes
        src: "/usr/share/doc/fcgiwrap/examples/nginx.conf"
        dest: "/etc/nginx/fcgiwrap.conf"

    - name: "Delete default site"
      file:
        path: "/etc/nginx/sites-enabled/default"
        state: absent

    - name: "Set up nginx host config"
      template:
        src: "nginx-config"
        dest: "/etc/nginx/sites-available/x2go"
        owner: "root"
        group: "root"
        mode: "0644"

    - name: "Set directory permissions"
      file:
        path: "{{ item | dirname }}"
        mode: "o+x"
        recurse: yes
      loop:
        - "{{ web_ssl_cert }}"
        - "{{ web_ssl_key }}"

    - name: "Set files' owner and permission"
      file:
        path: "{{ item }}"
        owner: "www-data"
      loop:
        - "{{ web_ssl_cert }}"
        - "{{ web_ssl_key }}"

    - name: "Activate nginx host config"
      file:
        src: "../sites-available/x2go"
        dest: "/etc/nginx/sites-enabled/x2go"
        state: link

    - name: "Restart nginx service"
      systemd:
        name: "nginx"
        state: restarted
