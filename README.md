# x2go HTML client

This ansible role strives to set up an x2go HTML client server as described here:
[https://wiki.x2go.org/doku.php/wiki:advanced:x2gohtmlclient](https://wiki.x2go.org/doku.php/wiki:advanced:x2gohtmlclient)

Unfortunately, the documentation is really skimpy, so it might be a bit of guess-work.
